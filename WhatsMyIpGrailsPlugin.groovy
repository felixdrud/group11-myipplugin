class WhatsMyIpGrailsPlugin {
    // the plugin version
    def version = "0.2"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.5 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    def title = "Whats My Ip Plugin" // Headline display name of the plugin
    def author = "Felix E. Drud"
    def authorEmail = "felix.drud@gmail.com"
    def description = '''\
Provides a easy taglib and service to show the users IP adress, using the JSON REST service http://jsonip.com/.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/whats-my-ip"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
//    def license = "APACHE"

    // Details of company behind the plugin (if there is one)
    def organization = [ name: "Group 11, DM844, SDU", url: "http://athena.drud.org/" ]

    // Any additional developers beyond the author specified above.
//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

    // Location of the plugin's issue tracker.
//    def issueManagement = [ system: "JIRA", url: "http://jira.grails.org/browse/GPMYPLUGIN" ]

    // Online location of the plugin's browseable source code.
    def scm = [ url: "https://bitbucket.org/felixdrud/group11-myipplugin/" ]

}
