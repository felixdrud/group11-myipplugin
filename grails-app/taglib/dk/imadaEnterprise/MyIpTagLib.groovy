package dk.imadaEnterprise

class MyIpTagLib {
    static defaultEncodeAs = [taglib:'none']
    static namespace = "whatsmyip"
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    MyIpService myIpService

    def myIp = { attrs , body ->
        out << body() << myIpService.userIP
    }
}
