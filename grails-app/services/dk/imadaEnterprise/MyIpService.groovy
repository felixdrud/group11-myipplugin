package dk.imadaEnterprise

import grails.transaction.Transactional
import groovy.json.JsonSlurper

@Transactional
class MyIpService {

	private String getUsersIp() {
		String json = "http://ip-api.com/json".toURL().text

		JsonSlurper slurper = new JsonSlurper()
		Map parsedJSON = slurper.parseText(json)

		return parsedJSON.query
	}

    String getUserIP() {
		String ip
		try {
			ip = usersIp
		} catch (Exception e) {
			log.error("Couldn't reach jsonip.com service.")
			ip = "127.0.0.1"
		}
		return ip
    }
}
